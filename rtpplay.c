/* rtpplay -- Pickup UDP packets, remove header and play.
 *
 * This is a very simple RTP-header-stripper to be able
 * to play the stuff sent to it by retrocast or other
 * RTP utilities.  It can be used to debug SIP and other
 * media-based utilities before their session functions
 * are complete.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <errno.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>


/* Global variables */

int rtpsox;
FILE *playfh;

struct rtppkt {
	uint16_t V_P_X_CC_M_PT;
	uint16_t seqnr;
	uint32_t tstamp;
	uint32_t syncrsc;
	uint8_t payload [65536];
};
struct rtppkt rtppkt;



void mainloop (void) {
	ssize_t recvd;
	size_t sent;
	while (true) {
		recvd = recv (rtpsox, &rtppkt, sizeof (rtppkt), 0);
		if (recvd == -1) {
			perror ("Receive error");
			exit (1);
		}
		if (recvd > 12) {
			sent = fwrite (rtppkt.payload, 1, recvd - 12, playfh);
			if (sent == -1) {
				perror ("Write error");
				return;
			}
			if (sent != recvd - 12) {
				fprintf (stderr, "Received %zd sample bytes, but only able to send out %zd\n", recvd, sent);
			}
		}
	}
}

int main (int argc, char *argv []) {
	struct sockaddr_in6 sa6;
	if (argc != 3) {
		fprintf (stderr, "Usage: %s bind_ip6 bind_rtpport\n", argv [0]);
		exit (1);
	}
	playfh = popen ("play -t al -", "w");
	if (!playfh) {
		fprintf (stderr, "Failed to start player\n");
		exit (1);
	}
	rtpsox = socket (AF_INET6, SOCK_DGRAM, 0);
	if (rtpsox == -1) {
		perror ("No socket");
		exit (1);
	}
	bzero (&sa6, sizeof (sa6));
	sa6.sin6_family = AF_INET6;
	sa6.sin6_port = htons (atoi (argv [2]));
	if (inet_pton (AF_INET6, argv [1], &sa6.sin6_addr) == -1) {
		fprintf (stderr, "Failed to parse \"%s\" as an IPv6 address\n", argv [1]);
		exit (1);
	}
	if (bind (rtpsox, (struct sockaddr *) &sa6, sizeof (sa6)) == -1) {
		perror ("Address error");
		exit (1);
	}
	mainloop ();
	close (rtpsox);
	pclose (playfh);
	rtpsox = -1;
	exit (errno? 1: 0);
}

