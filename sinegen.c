/* sinegen.c -- because Python3 makes byte-level manipulation unworkable
 *
 * Generate sine values as 16-bit signed integers, and convert them to A-law
 * coded data.  This was originally written in Python2, but easier in C.
 * 
 * Using C instead of Python3 may seem silly, but the binary/text distinction
 * made Python3 skew solidly towards text processing and away from practical
 * quick hacks on bits and bytes.  No judgement here, Python3 is wonderful in
 * many ways, but for bit hacks it is now easier to fall back on C.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#include <unistd.h>


void sine2alaw (unsigned samplecount, int *sine_in, uint8_t *sine_out) {
	//
	// Iterate over samples, one at a time
	while (samplecount-- > 0) {
		//
		// Fetch the int sample
		int in = *sine_in++;
		uint8_t out = 0;
fprintf (stderr, "%04x --> ", in & 0x0000ffff);
		//
		// Map the sign bit
		if (in < 0) {
			in = -in;
		} else {
			out |= 0x80;
		}
fprintf (stderr, "sig %d, val %04x, ", out >> 7, in);
		//
		// Find the range
		int shift = 14;
		while (shift > 7) {
			if ((in >> shift) > 0) {
				break;
			}
			shift--;
		}
		//
		// Map in the exponent
fprintf (stderr, "exp %d, ", shift - 7);
		out |= (shift - 7);
		if (shift == 7) {
			shift = 8;
		}
		//
		// Shift the mantisse into the low in bits
		in >>= ( shift - 4 );
fprintf (stderr, "mant %x\n", in & 0x0f);
		//
		// Append the parts following the exponent
		out |= (in & 0x0f);
		//
		// Write the A-law sample
		*sine_out++ = out;
	}
}


void alaw2cdef (char *varnm, unsigned samplecount, uint8_t *sine_alaw) {
	printf ("uint8_t %s [%d] = {", varnm, samplecount);
	char *comma = "";
	while (samplecount-- > 0) {
		printf ("%s 0x%02x", comma, *sine_alaw++);
		comma = ", ";
	}
	printf (" };\n");
}

int sin0 [20];
int sin1200 [20];
int sin2400 [20];
int sin425 [40];

uint8_t alaw0 [20];
uint8_t alaw1200 [20];
uint8_t alaw2400 [20];
uint8_t alaw425 [40];

void init_sine_alaw (unsigned samplecount, int *sine, uint8_t *alaw, float volume, float frequency) {
	for (unsigned i = 0; i < samplecount; i++) {
		sine [i] = round (volume * sin (2 * M_PI * i * frequency / 8000.0));
	}
	sine2alaw (samplecount, sine, alaw);
}

void init_all (void) {
	init_sine_alaw (20, sin0,    alaw0,        0.0, 1200.0);
	init_sine_alaw (20, sin1200, alaw1200, 20000.0, 1200.0);
	init_sine_alaw (20, sin2400, alaw2400, 20000.0, 2400.0);
	init_sine_alaw (40, sin425,  alaw425,  20000.0,  425.0);
}

int main (int argc, char *argv []) {
	init_all ();
	for (int i = 0; i < 250; i++) {
		write (1, alaw425, sizeof (alaw425));
	}
	for (int i = 0; i < 1000; i++) {
		write (1, alaw0, sizeof (alaw0));
	}
	for (int i = 0; i < 500; i++) {
		write (1, alaw1200, sizeof (alaw1200));
	}
	for (int i = 0; i < 1000; i++) {
		write (1, alaw0, sizeof (alaw0));
	}
	for (int i = 0; i < 500; i++) {
		write (1, alaw2400, sizeof (alaw2400));
	}
	for (int i = 0; i < 1000; i++) {
		write (1, alaw0, sizeof (alaw0));
	}
	for (int i = 0; i < 250; i++) {
		write (1, alaw425, sizeof (alaw425));
	}
}
