Basicast: Unicasting and Multicasting Basicode over RTP
=======================================================

This program is a bit of nostalgic fun, as well as a useful test tool for
submitting RTP streams with ISDN codecs.  It uses the famous old Basicode
sound format for submitting an ASCII file over RTP.

The sending mechanism is always over IPv6, and can target an anycast or
multicast address.  It will be sent to a given UDP port and to a given
RTP stream identifier.  When started, it will print SDP that could be
used to describe the stream.  It will actually consume incoming RTP
responses as well, since RTP streams are often symmetrical, but it will
not do anything with any received data.


Sound encoding
--------------

The following three pictures from http://www.nostalgia8.nl/basicode.htm
describe the format (but leaves a question about escaping the ETX character).
They are derived from the program ``bc2.com``.

.. image:: basicast-1.png


.. image:: basicast-2.png


.. image:: basicast-3.png

